var tinyMCEImageList = new Array(
    {% for image in object_list %}
    ["{{ image.name|escape }}", "{{ image.get_image_url }}"]{% for tn in image.thumbnail_set.all %},
    ["{{ tn|escape }}", "{{ tn.get_image_url }}"]{% endfor %}{% if not forloop.last %},{% endif %}
    {% endfor %}
);
