from decimal import Decimal

from danemco.misc.template import make_tag
from danemco.misc.templatetags.danemco_thumbnail import *
from danemco.utils import app_installed as is_installed
from django import template
from django.conf import settings
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.core.exceptions import MultipleObjectsReturned
from django.core.urlresolvers import resolve, Resolver404
from django.db.models import get_models
from django.template import Library, Node, resolve_variable, Variable, \
    VariableDoesNotExist
from django.template.defaultfilters import slugify
from django.template.defaulttags import LoadNode
from django.template.loader_tags import ConstantIncludeNode, IncludeNode
from django.utils.datastructures import SortedDict
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.utils.text import normalize_newlines
import logging
import math
import os
import re
import sys
from django.core.urlresolvers import resolve, Resolver404
import logging
from django.core.exceptions import MultipleObjectsReturned
from django.utils.datastructures import SortedDict
import urllib
from django.core.cache import cache
from django.template.defaultfilters import slugify
from fractions import Fraction

register = template.Library()


@register.filter()
def abbrev(string):
    return "".join([word[0] for word in force_unicode(string).split()])


@register.filter()
def strip(text, stripped_chars=None):
    return ("%s" % text).strip(stripped_chars)


@register.filter()
def rstrip(text, stripped_chars=None):
    return ("%s" % text).rstrip(stripped_chars)


@register.filter()
def lstrip(text, stripped_chars=None):
    return ("%s" % text).lstrip(stripped_chars)


@register.filter()
def remove_newlines(text):
    """
    Removes all newline characters from a block of text.
    """
    # First normalize the newlines using Django's nifty utility
    normalized_text = normalize_newlines(text)
    # Then simply remove the newlines like so.
    return mark_safe(normalized_text.replace('\n', ' '))
remove_newlines.is_safe = True


@register.tag('site')
@make_tag
def site():
    """
    Returns current site object
    usage:
    {% site %}
    {% site as site %}
    """
    return Site.objects.get_current()


@register.simple_tag
def DOMAIN():
    return 'http://' + Site.objects.get_current().domain


@register.simple_tag
def get_media_root():
    return settings.MEDIA_ROOT


@register.simple_tag
def is_devel():
    return settings.DEBUG


@register.filter
def is_velocity_site(request):
    try:
        return settings.VELOCITY_SITE
    except:
        return False


@register.simple_tag
def insert_flatpage_content(flatpage, append=''):
    """
    Load the content of a flatpage based on the flatpage name
    Example::
      {% insert_flatpage_content "Welcome Page" %}
    """
    content = ' '
    try:
        fp = FlatPage.objects.get(title=flatpage.lower() + append,
                                  sites=settings.SITE_ID)
        if fp.content is not None:
            content = fp.content
        else:
            content = "hello"
    except:
        pass

    return content


@register.filter
def app_installed(value):
    return is_installed(value)


@register.filter
def flatpage_requires_auth(flatpage):
    try:
        fp = FlatPage.objects.get(title=flatpage.lower(), sites=settings.SITE_ID)
        return fp.registration_required
    except:
        return False


@register.tag("flatpage_tree")
@make_tag
def flatpage_tree(startingwith="/", max_levels=None):
    """
     flatpage_tree: returns a list
    """
    try:
        pages = FlatPage.objects.filter(url__startswith=startingwith).order_by("title")
        tree = SortedDict()

        def place_in_tree(page, tree):
            for leaf in tree:
                if leaf.url.lstrip(startingwith) in page.url.lstrip(startingwith):
                    return place_in_tree(page, tree[leaf])
            tree[page] = SortedDict()
            return

        for page in pages:
            place_in_tree(page, tree)

        return tree
    except Exception, ex:
        return ex


@register.filter
def truncatechars(value, length=100):
    length = int(length)
    if len(value) > length:
        return u'%s...' % value[:length]
    return value


@register.simple_tag
def display_filename(filefield):
    return filefield.__unicode__().split('/')[-1]


@register.simple_tag
def action_admin_prefix(obj):
    try:
        if 'satchmo' in str(obj.get_edited_object().__module__):
            return '/admin/'
    except:
        pass
    return '/dash/'


def get_admin_app_list(parser, token):
    """
    Returns a list of installed applications and models for which the current user
    has at least one permission.

    Syntax::

        {% get_admin_app_list as [context_var_containing_app_list] %}

    Example usage::

        {% get_admin_app_list as admin_app_list %}
    """
    tokens = token.contents.split()
    if len(tokens) < 3:
        raise template.TemplateSyntaxError, "'%s' tag requires two arguments" % tokens[0]
    if tokens[1] != 'as':
        raise template.TemplateSyntaxError, "First argument to '%s' tag must be 'as'" % tokens[0]
    return AdminWithoutSatchmoApplistNode(tokens[2])

register.tag('get_admin_app_list_without_satchmo', get_admin_app_list)


@register.filter
def ceil(number):
    return int(math.ceil(float(str(number))))


class GetSiteDomainNode(template.Node):
    def __init__(self, varname):
        self.varname = varname

    def render(self, context):

        # domain = context['request'].META['SERVER_NAME']
        domain = context['request'].path
        context[self.varname] = domain

        return ''


@register.tag
def get_site_domain(parser, token):
    """
        {% get_site_domain as varname %}
    """

    tag, a, varname = token.contents.split()
    return GetSiteDomainNode(varname)


def do_include_ifapp(parser, token):
    """
    Loads a template and renders it with the current context if the specified
    application is in settings.INSTALLED_APPS.

    Example::

        {% includeifapp "satchmo_store.shop" "foo/some_include" %}
    """
    bits = token.split_contents()
    if len(bits) != 3:
        raise TemplateSyntaxError("%r tag takes two argument: the name of the application and the name of the template to be included" % bits[0])
    app_name, path = bits[1:]

    app_name = app_name.strip('"\'')
    if not app_installed(app_name):
        return template.Node()

    if path[0] in ('"', "'") and path[-1] == path[0]:
        return ConstantIncludeNode(path[1:-1])
    return IncludeNode(path)
# register.tag('includeifapp', do_include_ifapp)


@register.filter
def strstr(haystack, needle):
    try:
        location = str(haystack).find(needle)
    except:
        location = haystack.encode("ascii", "ignore").find(needle)
    return location >= 0


@register.filter
def remove_html_tags(data, wif=""):
    """ please use striptags from builtin django filters instead """
    p = re.compile(r'<.*?>')
    return p.sub(wif, data)


@register.filter
def striphtmltags(string, tags="script,object,embed,\?(php)?.*\?,\!--.*--"):
    """
    removes the [X]HTML tag and it's content,
    if you need to leave the content use the builtin django striptags
    usage:  {{ string|striphtmltags }}
            {{ string|striphtmltags:"script,object,a"
    """

    for tag in [tag.strip() for tag in tags.split(',') if not tag == '']:
        p = re.compile("</?%s(>|\s[^>]*>)(.+</%s(>|\s[^>]*>)|)" % (tag, tag), re.IGNORECASE)
        string = p.sub('', string)
    return string


@register.filter
def float_gte(amount1, amount2):
    return float(amount1) >= float(amount2)


@register.filter
def int_gte(amount1, amount2):
    return int(amount1) >= int(amount2)


@register.filter
def to_fraction(amount):
    whole, dec = divmod(float(amount), 1)
    if whole:
        val = str(int(whole))
    else:
        val = ""
    if dec > 0:
        val += u' %s' % Fraction(dec)
    return val


@register.filter
def normalize(number):
    """
    Remove Trailing zeros of a decimal.
    """
    if isinstance(number, Decimal):
        if number == number.to_integral():
            return number.quantize(Decimal(1))
        else:
            return number.normalize()
    return number


@register.tag("get_flatpage")
@make_tag
def get_flatpage(url):
    try:
        return FlatPage.objects.get(url=url)
    except:
        return None


@register.filter
def flatpage_exists(name):
    return FlatPage.objects.filter(title=name).count() > 0


@register.filter
def breadcrumbs(url):
    """
    creates a list of breadcrumbs from flatpages and views
    You can give a view a title by simply giving it the `title` attribute
    """

    append_slash = getattr(settings, "APPEND_SLASH", True)

    parts = url.strip("/").split("/")

    urls = []
    purl = "/"
    if append_slash:
        for idx, part in enumerate(parts):
            if part:
                purl += part + "/"
                urls.append(purl)
    else:
        # have to do twice as much work, for the folders might not end in slash
        for idx, part in enumerate(parts):
            if part:
                purl += part
                urls.append(purl)
                purl += "/"
                urls.append(purl)

    # get possible flatpagesChair-Bellevue
    fps = FlatPage.objects.filter(
        url__in=urls,
        sites=Site.objects.get_current()).values_list("url", "title")

    fps = dict(fps)

    # finallay build flatpages
    crumbs = []
    for purl in urls:
        if purl in fps:
            crumbs.append((purl, fps[purl]))
        else:
            try:
                view = resolve(purl).func
                if hasattr(view, "title"):
                    crumbs.append((purl, view.title))
            except (Resolver404, AttributeError):
                pass

    return crumbs


def cache_external(url):
    key = "external:%s" % url
    filepath = cache.get(key)
    if not filepath:
        file = urllib.urlopen(url)
        data = file.read()
        file.close()

        filepath = os.path.join(settings.MEDIA_ROOT, "EXTERNAL", slugify(url))

        try:
            os.makedirs(os.path.dirname(filepath))
        except OSError:
            pass

        file = open(filepath, "w")
        file.write(data)
        file.close()

        filepath = filepath.replace(settings.MEDIA_ROOT, settings.MEDIA_URL)
        filepath = filepath.replace("//", "/")

        cache.set(key, filepath)

    return filepath
register.filter(cache_external)
register.simple_tag(cache_external)


@register.filter
def user_in_group(user, group_name):
    g = False
    if user.is_authenticated():
        try:
            g = user.groups.get(name=group_name)
        except Exception, ex:
            g = ex
    return g


@register.filter
def order_by(queryset, args):
    args = [x.strip() for x in args.split(',')]
    return queryset.order_by(*args)


@register.inclusion_tag('misc/login_link.html', takes_context=True)
def login_link(context, login_text="Login", logout_text="Logout"):
    logged_in = False
    if "request" in context:
        logged_in = context["request"].user.is_authenticated()
    return locals()


class SetVariable(template.Node):
    def __init__(self, vars):
        self.vars = vars

    def render(self, context):
        for name, value in self.vars:
            try:
                var = Variable(value).resolve(context)
            except VariableDoesNotExist:
                var = None
            context[name] = var
        return ''


@register.tag()
def assign(parser, token):
    from re import split
    bits = split(r'\s+', token.contents)
    bits = [bit.split("=") for bit in bits[1:]]
    return SetVariable(bits)


class SetCache(template.Node):
    def __init__(self, name, value, keys):
        self.name = name
        self.value = value
        self.keys = keys

    def render(self, context):
        key = "_setcache-templatetag"
        for kv in self.keys:
            key += ":%s" % kv

        val = cache.get(key)
        if not val:
            try:
                val = Variable(self.value).resolve(context)
                cache.set(key, val)
            except VariableDoesNotExist:
                val = None
        context[self.name] = val
        return ''


@register.tag()
def cachefetch(parser, token):
    from re import split
    bits = split(r'\s+', token.contents)[1:]
    name, value = bits[0].split("=")
    return SetCache(name, value, bits[1:])


@register.tag("querylist")
@make_tag
def querylist():
    import operator
    from django.db import connection
    return sorted(connection.queries, key=operator.itemgetter("time"), reverse=True)
