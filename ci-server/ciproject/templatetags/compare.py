from django.template import Library
from decimal import Decimal
from django.utils.encoding import smart_unicode

register = Library()

import logging

@register.filter
def split(str,splitter):
    return str.split(splitter)

@register.filter
def typeof(obj1):
    return unicode(type(obj1))

def apples2apples(obj1, obj2):
	return obj1.__class__(obj2)

@register.filter
def multiply(obj1, obj2):
    return Decimal(str(obj1)) * Decimal(str(obj2))

@register.filter
def divide(obj1, obj2):
    return Decimal(str(obj1)) / Decimal(str(obj2))

@register.filter
def addfloat(obj1, obj2):
	try:
		f1 = float(str(obj1))
	except ValueError:
		f1 = 0.0
	try:
		f2 = float(str(obj2))
	except ValueError:
		f2 = 0.0

	retval = f1 + f2
	retval = Decimal(str(retval))
	return retval

@register.filter
def min(obj1, obj2):
	retval = float(str(obj1))
	if retval < float(str(obj2)):
		retval = Decimal(str(obj2))
	return retval

@register.filter
def sub(obj1, obj2):
	return obj1 - obj2

@register.filter
def gt(obj1, obj2):
	return obj1 > apples2apples(obj1, obj2)

@register.filter
def lt(obj1, obj2):
	return obj1 < apples2apples(obj1, obj2)

@register.filter
def gte(obj1, obj2):
	return obj1 >= apples2apples(obj1, obj2)

@register.filter
def lte(obj1, obj2):
	return obj1 <= apples2apples(obj1, obj2)

@register.filter
def equal(obj1, obj2):
	return obj1 == apples2apples(obj1, obj2)

@register.filter
def startswith(a, b):
    return smart_unicode(a).startswith(smart_unicode(b))

@register.filter
def endswith(a, b):
    return smart_unicode(a).endswith(smart_unicode(b))

@register.filter
def template_dir(obj1):
	return "%s" % dir(obj1)

@register.filter
def equals(a, b):
    return a == b
