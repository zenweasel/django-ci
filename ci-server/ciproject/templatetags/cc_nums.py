from django.template import Library

register = Library()

@register.filter
def safe_ccnum(ccnum):
    try:
        return unicode(ccnum)
    except Exception, ex:
        return ex