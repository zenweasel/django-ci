from datetime import datetime, timedelta
from decimal import *

from django import template
from django.conf import settings
from django.db.models import Sum
from django.utils.datastructures import SortedDict

register = template.Library()


if 'satchmo_store.shop' in settings.INSTALLED_APPS:
    from satchmo_store.shop.models import Order,OrderItem
    from product.models import Product

    def sales_totals(frequency="daily",days=30, date=None):
        """Output JSON formatted template with sales totals data
        for frequency daily, weekly, monthly for any number of days back from a specified date"""
        try:
            end_date = datetime.strptime(date, '%m/%d/%Y')
        except:
            end_date = datetime.now()
        start_date = end_date - timedelta(days=days)
        orders = Order.objects.filter(time_stamp__gt=start_date)
        if orders:
            total_sales = orders.aggregate(Sum("total"))['total__sum'].quantize(Decimal('.01'), rounding=ROUND_UP)
        else:
            total_sales = 0
    
        if frequency == "daily":
            daily_totals = {}
            for d in range(days,0,-1):
                day = end_date - timedelta(days=d)
                day_orders = Order.objects.filter(time_stamp__year=day.year).filter(time_stamp__month=day.month).filter(time_stamp__day=day.day)
                if len(day_orders):
                    day_total_sales = day_orders.aggregate(Sum("total"))['total__sum'].quantize(Decimal('.01'), rounding=ROUND_UP)
                    daily_totals[day] = day_total_sales
                else:
                    daily_totals[day] = 0
            return {
                'daily_totals': daily_totals,
                'end_date': end_date,
                'start_date': start_date,
                'orders': orders,
                'total_sales': total_sales,
                }
    # elif frequency == "weekly":
    #     #partial weeks count as a week
    #     weeks = days/7 + bool(days%7)
    #     for week in range(weeks,0,-1):
    #         day1 = start_date.date() - timedelta(days=week*7)
    #         day7 = start_date.date() - timedelta(days=week*7-6)
    #         week_orders = Order.objects.filter(time_stamp__gte=day1).filter(time_stamp__lte=day7)
    #         week_total_sales = week_orders.aggregate(Sum("total"))['total__sum'].quantize(Decimal('.01'), rounding=ROUND_UP)
    #         weekly_totals[day1] = week_total_sales
    # elif frequency == "monthly":
    #     # TODO
    #     pass

    register.inclusion_tag('shop/admin/sales_totals.html')(sales_totals)


    def order_numbers(frequency="daily",days=30, date=None):
        """Output JSON formatted template with order numbers data
        for frequency daily, weekly, monthly for any number of days back from a specified date"""
        try:
            end_date = datetime.strptime(date, '%m/%d/%Y')
        except:
            end_date = datetime.now()
        start_date = end_date - timedelta(days=days)
        orders = Order.objects.filter(time_stamp__gt=start_date)
    
        if frequency == "daily":
            daily_count = {}
            for d in range(days,0,-1):
                day = end_date - timedelta(days=d)
                day_orders = Order.objects.filter(time_stamp__year=day.year,time_stamp__month=day.month,time_stamp__day=day.day)
                day_count = len(day_orders)
                daily_count[day] = day_count

            return {
                'daily_count': daily_count,
                'total_count': sum(daily_count.values()),
                'end_date': end_date,
                'start_date': start_date,
                'orders': orders,
                }

    register.inclusion_tag('shop/admin/order_numbers.html')(order_numbers)


    def top_grossing_products(days=30, date=None):
        """Output JSON formatted template with order numbers data
        for any number of days back from a specified date"""

        try:
            end_date = datetime.strptime(date, '%m/%d/%Y')
        except:
            end_date = datetime.now()
        start_date = end_date - timedelta(days=days)
        orders = Order.objects.filter(time_stamp__gt=start_date)

        products = Product.objects.filter(orderitem__order__in=orders).distinct().annotate(
            line_total=Sum("orderitem__line_item_price"),
            )
        return {
            'products': products,
            'end_date': end_date,
            'start_date': start_date,
            'orders': orders,
            }

    register.inclusion_tag('shop/admin/top_grossing_products.html')(top_grossing_products)

else:
    def sales_totals(frequency="daily",days=30, date=None):
        return {}
    register.inclusion_tag('shop/admin/sales_totals.html')(sales_totals)
    def order_numbers(frequency="daily",days=30, date=None):
        return {}
    register.inclusion_tag('shop/admin/order_numbers.html')(order_numbers)
    def top_grossing_products(days=30, date=None):
        return {}
    register.inclusion_tag('shop/admin/top_grossing_products.html')(top_grossing_products)
    
#custom filter to help sort python dictionaries in the templates
@register.filter(name='sort')
def listsort(value):
        if isinstance(value, dict):
            new_dict = SortedDict()
            key_list = value.keys()
            key_list.sort()
            for key in key_list:
                new_dict[key] = value[key]
            return new_dict
        elif isinstance(value, list):
            new_list = list(value)
            new_list.sort()
            return new_list
        else:
            return value
        listsort.is_safe = True
