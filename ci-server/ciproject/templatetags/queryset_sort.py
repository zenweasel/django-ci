from django.template import Library, Node
from UserDict import UserDict

register = Library()

@register.filter
def sort(queryset, sort_field):
	return queryset.order_by(sort_field)




@register.filter
def limit(queryset, limit_to, offset=None):
	if offset:
		queryset = queryset[offset:(offset+limit_to)]
	else:
		queryset = queryset[:limit_to]
	return queryset

@register.filter
def manual_distinct(queryset):
	"""
	 Since doing an order by can have strange results when added with distinct
	 this is the way to manually ensure that they are correctly distinct
	"""
	ordered_dict = odict()
	for item in queryset:
		ordered_dict[item.id] = item
	return ordered_dict.values()
	
class odict(UserDict):
    def __init__(self, dict = None):
        self._keys = []
        UserDict.__init__(self, dict)

    def __delitem__(self, key):
        UserDict.__delitem__(self, key)
        self._keys.remove(key)

    def __setitem__(self, key, item):
        UserDict.__setitem__(self, key, item)
        if key not in self._keys: self._keys.append(key)

    def clear(self):
        UserDict.clear(self)
        self._keys = []

    def copy(self):
        dict = UserDict.copy(self)
        dict._keys = self._keys[:]
        return dict

    def items(self):
        return zip(self._keys, self.values())

    def keys(self):
        return self._keys

    def popitem(self):
        try:
            key = self._keys[-1]
        except IndexError:
            raise KeyError('dictionary is empty')

        val = self[key]
        del self[key]

        return (key, val)

    def setdefault(self, key, failobj = None):
        UserDict.setdefault(self, key, failobj)
        if key not in self._keys: self._keys.append(key)

    def update(self, dict):
        UserDict.update(self, dict)
        for key in dict.keys():
            if key not in self._keys: self._keys.append(key)

    def values(self):
        return map(self.get, self._keys)
