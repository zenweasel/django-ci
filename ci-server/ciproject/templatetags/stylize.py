from django.template import Library, Node, resolve_variable
from pygments import highlight
from pygments.lexers import get_lexer_by_name, PythonLexer
from pygments.formatters import HtmlFormatter
from django.utils.safestring import mark_safe
from django.conf import settings
from django.utils.encoding import smart_str, force_unicode

register = Library()

# usage: {% stylize "language" %}...language text...{% endstylize %}
class StylizeNode(Node):
    def __init__(self, nodelist, *varlist):
        self.nodelist, self.vlist = (nodelist, varlist)

    def render(self, context):
        style = 'text'
        if len(self.vlist) > 0:
            style = resolve_variable(self.vlist[0], context)
        html = self.nodelist.render(context)
        lexer = get_lexer_by_name(style)        
        return highlight(html, lexer, HtmlFormatter(linenos='table'))

def stylize(parser, token):
    nodelist = parser.parse(('endstylize',))
    parser.delete_first_token()
    return StylizeNode(nodelist, *token.contents.split()[1:])

stylize = register.tag(stylize)

@register.filter()
def rst(text):
    from docutils.core import publish_parts
    from docutils.writers import html4css1
    from docutils.parsers.rst import roles
    from docutils import nodes
    from docutils import nodes
    from docutils.parsers.rst import directives
    
    INLINESTYLES = False
    
    def pygments_directive(name, arguments, options, content, lineno,
                           content_offset, block_text, state, state_machine):
        try:
            lexer = get_lexer_by_name(arguments[0])
        except ValueError:
            # no lexer found - use the text one instead of an exception
            lexer = PythonLexer()
            
        # take an arbitrary option if more than one is given
        parsed = highlight(u'\n'.join(content), lexer, HtmlFormatter(linenos='table'))
        
        return [nodes.raw('', parsed, format='html')]    
    pygments_directive.arguments = (1, 0, 1)
    pygments_directive.content = 1    
    directives.register_directive('code', pygments_directive)
    
    def link_role(name, rawtext, text, lineno, inliner,
            options={}, content=[]):
        name = text.replace("_", " ")
        return [nodes.reference(rawtext, name, refuri='?filename=%s.txt' % text)], []
    
    roles.register_local_role("link", link_role)
            
    docutils_settings = getattr(settings, "RESTRUCTUREDTEXT_FILTER_SETTINGS", {})
    parts = publish_parts(source=smart_str(text), writer=html4css1.Writer(),
            settings_overrides=docutils_settings)
    
    return mark_safe(force_unicode(parts["fragment"]))