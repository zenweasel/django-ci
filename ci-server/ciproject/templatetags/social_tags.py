from django import template
from django.contrib.sites.models import Site
register = template.Library()


@register.inclusion_tag('social/social_network_links.html')
def get_social_links():
        links = SocialLink.objects.all()
        return {'social_links': links}


@register.inclusion_tag('social/social_network_apis.html')
def social_network_apis(facebook=True, google=True, twitter=True):
    """
    This should be placed in the header of the page to load the correct social network information

    Example::
      ...
      {% social_network_apis %}
      </head>

    """
    return locals()


@register.inclusion_tag('social/share_links.html')
def share_links(url=None, layout='horizontal', facebook=True, google=True, twitter=True):
    """
    display share links from facebook, google, and twitter
    This will call the facebook_like, google_plus and tweet template tags

    you can pass in a url to share otherwise it uses the current url
    you can also change the layout from horizontal to vertical

    by default it shows in horizontal format

    you should use {% social_network_apis %} in the head to use this.

    Syntax::

      {% share_links url layout %}

    Examples::

      {% share_links %}

      {% share_links 'http://google.com' 'vertical' %}

    """
    if layout == 'vertical':
        html_tag = 'div'
    else:
        html_tag = 'span'
    return locals()


@register.inclusion_tag('social/facebook_like.html')
def facebook_like(url=None, layout="button_count", width=100, action='like', font=None, show_faces=False, send=False):
    """
    put in a like button on the page. This uses the iframe version of the code.
    By default use the current url unless one is passed in.

    To see the possible values for the options check out
    http://developers.facebook.com/docs/reference/plugins/like/

    Syntax::

      {% facebook_like url layout width action font show_faces send %}

    Examples::

      {% facebook_like %}

      {% facebook_like 'http://google.com' 'standard' 200 'recommend' 'Arial' 1 1 %}

    """
    if layout == "button_count":
        height = 20
    elif layout == "box_count":
        height = 62
    else:
        layout = "standard"
        height = 80
    return locals()


@register.inclusion_tag('social/google_plus.html')
def google_plus(url=None, size=None, hide_count=False):
    """
    Insert a google 1+ icon

    To see the possible values for the options check out
    http://www.google.com/webmasters/+1/button/

    you should use {% social_network_apis %} in the head to use this.

    Syntax::

      {% google_plus url size hide_count %}

    Examples::

      {% google_plus %}

      {% google_plus 'http://google.com' 'small' 1 %}

    """
    if size and size not in ('small', 'medium', 'tall'):
        size = None
    return locals()


@register.inclusion_tag('social/twitter.html')
def tweet(url=None, count="horizontal", text=None):
    """
    Show the tweet button on page.

    To see the possible values for the options check out
    http://twitter.com/about/resources/tweetbutton

    you should use {% social_network_apis %} in the head to use this.

    Syntax::

      {% tweet url count text %}

    Examples::

      {% tweet %}

      {% tweet 'http://google.com' 'none' 'Tweet text' %}

    """
    if count not in ('vertical', 'horizontal', 'none'):
        count = 'horizontal'

    return locals()


@register.inclusion_tag('social/pinterest.html')
def pinterest(url, img, description=None, count_layout="none"):
    """
    Show the Pinterest Button

    To see the possible values for the options check out
    http://pinterest.com/about/goodies/

    Syntax::

      {% pinterest url img_url description %}

    Examples::

      {% pinterest request.path product.main_image.picture.url product.short_description %}

    """
    if url.startswith("/"):
        url = url[1:]
    site = Site.objects.get_current()
    url = "http://%s/%s" % (site.domain, url)
    img = "http://%s%s" % (site.domain, img)
    return locals()


@register.filter
def extra_urlencode(string):
    return string.replace("/", "%2F")
