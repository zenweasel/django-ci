import os
from django import template
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
import cgi

register = template.Library()

@register.filter
def sub(a, b):
    return a - b

@register.filter
def mod(a, b):
    return a % b

def get(obj, value):
    if hasattr(obj, "get"):
        return obj.get(value)
    else:
        try:
            return obj[value]
        except:
            return None
register.filter(get)

def has(obj, value):
    return str(value) in obj
register.filter(has)

def attr(obj, value):
    retval = getattr(obj, value)
    if hasattr(retval, '__call__'):
        return retval()
    else:
        return retval
register.filter(attr)

def next(page, attr="pk"):
    if next.item == None:
        next.item = 0
    obj = page[next.item]
    next.item += 1
    if next.item >= len(page):
        next.item = None
    return getattr(obj, attr)
next.item = None
register.filter(next)

def inspect(obj):
    output = "<h3>%s</h3><table><thead><tr><td>Name</td><td>Value</td></tr></thead>" % str(obj.__class__)
    for item in dir(obj):
        try:
            output += "<tr><td>%s</td><td>%s</td></tr>" % (item, cgi.escape(str(getattr(obj, item)), True))
        except Exception, ex:
            output += "<tr><td>%s</td><td colspan='2'>%s</td></tr>" % (item, ex)
    output += "</table>"
    return mark_safe(output)
register.filter(inspect)

def dump(obj):
    output = "<h3>%s</h3><table><thead><tr><td>Name</td><td>Value</td></tr></thead>" % str(obj.__class__)
    for item in obj:
        try:
            output += "<tr><td>%s</td><td>%s</td></tr>" % (item, cgi.escape(str(obj[item])), )
        except Exception, ex:
            pass
    return mark_safe(output)
register.filter(dump)

@register.filter
def basename(obj):
    return os.path.basename(obj)
