from django import template
from django.template import Library, Node, resolve_variable, RequestContext
from django.db.models import get_models
from satchmo.shop.models import Cart, Product
from danemco.useraccess.models import UserAccess

register = template.Library()

def get_club_in_cart(order):

    club_product_list = Product.objects.filter(slug__icontains='-club_')

    for item in order.orderitem_set.all():
        if item.product in club_product_list:
            return item.product

    return None
        
def is_club_in_cart(cart):

    club_product_list = Product.objects.filter(slug__icontains='-club_')

    for order_item in cart:
        if order_item.product in club_product_list:
            return True

    return False

@register.filter
def only_one_club_in_cart(cart):
    club_product_list = Product.objects.filter(slug__icontains='-club_')

    club_count = 0
    for order_item in cart:
        if order_item.product in club_product_list:
            club_count = club_count + order_item.quantity

    if club_count <= 1:
        return True
    else:
        return False
    
class show_checkout_form_node(template.Node):
    def __init__(self, user, varname):
        self.user = user
        self.varname = varname

    def render(self, context):

        cart = Cart.objects.from_request(context['request'])
        
        club_in_cart = is_club_in_cart(cart)

        try:
            user = resolve_variable(self.user, context)
        except:
            return False

        if user.id == None and club_in_cart:
            return_val = False
        else:
            return_val = True
            
        context[self.varname] = return_val
      
        return ''

@register.tag
def show_checkout_form(parser, token):
  
    tag, user, a, varname = token.contents.split()
    return show_checkout_form_node(user,varname)

class update_shopper_goups_node(template.Node):
    def __init__(self, user, order):
        self.user = user
        self.order = order

    def render(self, context):

        # Get the order info.
        try:
            order = resolve_variable(self.order, context)
        except:
            return ''
        
        club = get_club_in_cart(order)

        if not club:
            return ''
        else:
            
            # Get the user object.
            try:
                user = resolve_variable(self.user, context)
            except:
                return ''

            # If user is AnonymousUser then return (this should never happen)
            if user.id == None:
                return ''
            else:
                try:
                    user_shoppergroup   = UserShopperGroup.objects.get(user=user)
                except:
                    user_shoppergroup       = UserShopperGroup()
                    user_shoppergroup.user = user

                try:
                    shoppergroup        = ShopperGroup.objects.get(name=club.name)
                except:
                    return ''

                user_shoppergroup.group = shoppergroup
                user_shoppergroup.save()
                
                return ''
        
@register.tag
def update_shopper_goups(parser, token):
    
    tag, user, order = token.contents.split()
    return update_shopper_goups_node(user, order)
    