from django import template
from django.conf import settings

from datetime import date
from datetime import datetime
from datetime import timedelta
from django.db.models import Sum
from django.db import connection
from decimal import *
from django.utils.datastructures import SortedDict

today = date.today
register = template.Library()


if 'satchmo_store.shop' in settings.INSTALLED_APPS:
    from satchmo_store.shop.models import Order, OrderItem
    from product.models import Product

    def google_sales_totals(frequency="daily", days=30, date=None):
        """Output JSON formatted template with sales totals data
        for frequency daily, weekly, monthly for any number of days back from a specified date"""
        if date:
            end_date = datetime.strptime(date, '%m/%d/%Y')
        else:
            end_date = datetime.now()
        start_date = end_date - timedelta(days=days)
        cursor = connection.cursor()
        cursor.execute('SELECT Count(Date(time_stamp)) as `sale_count`,Sum(total) AS `t_total`, DATE(time_stamp) AS `day_stamp` FROM `shop_order` WHERE `status` IN("New","Shipped","Complete", "In Process" ) and `shop_order`.`time_stamp` > %s group by `day_stamp` ORDER BY `day_stamp` ASC', [start_date])
        orders = cursor.fetchall()

        if frequency == "daily":
            daily_totals = SortedDict()
            biggest_sale = 0
            most_sales = 0
            total_count = 0
            total_sales = 0
            for d in range(days, -1, -1):
                day = end_date - timedelta(days=d)
                daily_totals[day.date()] = {'count': 0, 'total': 0}

            for count, total, day_stamp in orders:
                total = total or 0
                daily_totals[day_stamp] = {'count': count, 'total': total}
                biggest_sale = max(biggest_sale, total)
                total_sales += total
                most_sales = max(most_sales, count)
                total_count += count
            return {
                'daily_totals': daily_totals,
                'total_sales': total_sales,
                'total_count': total_count,
                'end_date': end_date,
                'start_date': start_date,
                'maximum_value': biggest_sale,
                'maximum_count': most_sales,
            }

    register.inclusion_tag('misc/charts/google_sales_totals.html')(google_sales_totals)

    def google_order_numbers(frequency="daily", days=30, date=None):
        """Output JSON formatted template with order numbers data
        for frequency daily, weekly, monthly for any number of days back from a specified date"""
        if date:
            end_date = datetime.strptime(date, '%m/%d/%Y')
        else:
            end_date = datetime.now()
        start_date = end_date - timedelta(days=days)
        cursor = connection.cursor()
        cursor.execute('SELECT (Count(Date(time_stamp))) AS `total`, (DATE(time_stamp)) AS `day_stamp` FROM `shop_order` WHERE `status` IN("New","Shipped","Complete", "In Process" ) and  `shop_order`.`time_stamp` > %s group by `day_stamp` ORDER BY `day_stamp` ASC', [start_date])
        orders = cursor.fetchall()

        if frequency == "daily":
            daily_count = SortedDict()
            biggest_sale = 0
            for d in range(days, -1, -1):
                day = end_date - timedelta(days=d)
                daily_count[day.date()] = 0

            for total, day_stamp in orders:
                total = total or 0
                daily_count[day_stamp] = total
                biggest_sale = max(biggest_sale, total)
            return {
                'daily_count': daily_count,
                'total_count': sum(daily_count.values()),
                'end_date': end_date,
                'start_date': start_date,
                'maximum_value': biggest_sale,
            }

    register.inclusion_tag('misc/charts/google_order_numbers.html')(google_order_numbers)

    def google_top_grossing_products(days=30, date=None, limit=10):
        """Output JSON formatted template with order numbers data
        for any number of days back from a specified date"""
        if date:
            end_date = datetime.strptime(date, '%m/%d/%Y')
        else:
            end_date = datetime.now()
        start_date = end_date - timedelta(days=days)
        products = Product.objects.filter(orderitem__order__time_stamp__gt=start_date).distinct().values('name').annotate(
            line_total=Sum("orderitem__line_item_price"),
        ).order_by('-line_total')[:limit]
        return {
            'products': products,
            'end_date': end_date,
            'start_date': start_date,
            'limit': limit,
        }

    register.inclusion_tag('misc/charts/google_top_grossing_products.html')(google_top_grossing_products)

else:
    def google_sales_totals(frequency="daily", days=30, date=today):
        return {}
    register.inclusion_tag('misc/charts/google_sales_totals.html')(google_sales_totals)

    def google_order_numbers(frequency="daily", days=30, date=today):
        return {}
    register.inclusion_tag('misc/charts/google_order_numbers.html')(google_order_numbers)

    def google_top_grossing_products(days=30, date=today, limit=10):
        return {}
    register.inclusion_tag('misc/charts/google_top_grossing_products.html')(google_top_grossing_products)


# custom filter to help sort python dictionaries in the templates
@register.filter(name='sort')
def listsort(value):
        if isinstance(value, dict):
            new_dict = SortedDict()
            key_list = value.keys()
            key_list.sort()
            for key in key_list:
                new_dict[key] = value[key]
            return new_dict
        elif isinstance(value, list):
            new_list = list(value)
            new_list.sort()
            return new_list
        else:
            return value
        listsort.is_safe = True
