import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ciproject.settings")

if __name__ == "__main__":
    from socketio_server.server import serve
    serve(('0.0.0.0', 7000))#, auto_reload=True)  # "mysite.sock")

